<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>簡易Twitter</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">

			<div class="header">

				<c:if test="${ empty loginUser }">
					<a href="login">ログイン</a>
					<a href="signup">登録する</a>
				</c:if>

				<c:if test="${ not empty loginUser }">
					<a href="./">ホーム</a>
					<a href="setting">設定</a>
					<a href="logout">ログアウト</a>
				</c:if>
			</div>

			<c:if test="${ not empty error }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${error}" var="error">
							<li><c:out value="${error}" />
						</c:forEach>
					</ul>
					<c:remove var="error" />
				</div>
			</c:if>

			<form action="./" method="get">
				<label for="date">日付</label>
				<input type="date" name="start" value="${start}" id="start">
				<input type="date" name="end" value="${end}" id="end">
				<input type="submit" value="絞り込み"><br/>
			</form>

			<c:if test="${ not empty loginUser }">

				<div class="profile">
					<div class="name">
						<h2>
							<c:out value="${loginUser.name}" />
						</h2>
					</div>

					<div class="account">
						<c:out value="${loginUser.account}" />
					</div>

					<div class="description">
						<c:out value="${loginUser.description}" />
					</div>
				</div>
			</c:if>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="form-area">
				<c:if test="${ isShowMessageForm }">
					<form action="message" method="post">いま、どうしてる？<br />
							<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea><br />
						<input type="submit" value="つぶやく">（140文字まで）
					</form>
				</c:if>
			</div>

			<c:if test="${ not empty editError }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${editError}" var="editError">
							<li><c:out value="${editError}" />
						</c:forEach>
					</ul>
					<c:remove var="editError" />
				</div>
			</c:if>

			<div class="messages">
				<c:forEach items="${messages}" var="message">
					<div class="message">
						<div class="account-name">
							<span class="account">
								<a href="./?user_id=<c:out value="${message.userId}"/> ">
									<c:out value="${message.account}" />
								</a>
							</span>

							<span class="name">
								<c:out value="${message.name}" />
							</span>
						</div>

						<div class="text"><c:out value="${message.text}" /></div>
						<div class="date">
							<fmt:formatDate value="${message.createdDate}"
								pattern="yyyy/MM/dd HH:mm:ss" />
						</div>

						<div>
							<c:if test="${message.userId == loginUser.id}">
									<form action="edit" method="get">
										<input name="messageId" type="hidden" value="${message.id}" />
										<input type="submit" value="編集">
									</form>

								<form action="deleteMessage" method="post">
									<input name="messageId" type="hidden" value="${message.id}" />
									<input type="submit" value="削除"><br />
								</form>
							</c:if>
						</div>

						<div>
							<c:if test="${ not empty commentError }">
								<div class="errorMessages">
									<ul>
										<c:forEach items="${commentError}" var="commentError">
											<li><c:out value="${commentError}" />
										</c:forEach>
									</ul>
									<c:remove var="commentError" />
								</div>
							</c:if>

							<c:if test="${ not empty loginUser }">
								<form action="comment" method="post">返信<br />

									<input name="messageId" type="hidden" value="${message.id}">
									<textarea name="comment" rows="3" cols="35"></textarea>
									<input type="submit" value="返信"><br/>

									<!-- itemsにServletから値を受け取り、varに代入
									そこからid等を指定して、条件式にあったものだけtop画面に表示する -->
									<c:forEach items="${comments}" var="comment">
										<c:if test="${message.id == comment.messageId}">
											<c:out value="${comment.name}" />
											<c:out value="${comment.account}" /><br/>
											<div class="comment"><c:out value="${comment.text}"/><br/></div>
											<fmt:formatDate value="${comment.createdDate}"
												pattern="yyyy/MM/dd HH:mm:ss" /><br/>
										</c:if>
									</c:forEach>
								</form>
							</c:if>
						</div>

					</div>
				</c:forEach>
			</div>

			<div class="copyright">Copyright(c)MasutaniDaiki</div>
		</div>
	</body>
</html>