<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>つぶやき編集画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>

	<body>
		<div class="main-contents">

			<form action="edit" method="post"><br />
				<textarea name="text" cols="100" rows="5" id="editText" class="tweet-box"><c:out value="${message.text}" />
				</textarea><br />（140文字まで）
				<input name="editId" type="hidden" value="${message.id}" /> <br />
				<input type="submit" value="更新">
			</form>
			<a href="./">戻る</a>
		</div>

		<div class="copyright">Copyright(c)MasutaniDaiki</div>
	</body>
</html>