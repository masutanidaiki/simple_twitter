package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//つぶやきをトップに表示するメソッド
	public List<UserMessage> select(String userId, String start, String end ) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();

			//ユーザーIDがServletから渡されたときidを整数型に変換
			//idがnullならつぶやき全件表示、値があればそのユーザーIDのつぶやきだけ表示
			Integer id = null;
			if(!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}

			//空文字・nullを判断するisBlank
			if(!StringUtils.isBlank(start)) {
				start = start + " 00:00:00";
			}else {
				start = "2021/01/01 00:00:00";
			}

			if(!StringUtils.isBlank(end)) {
				end = end + " 23:59:50";
			}else {
				//Calendar型(年月日時間を取得)で準備
				//getInstanceはnewと役割同じ
				Calendar dateObj = Calendar.getInstance();
				//受け取る値の型を指定する
				SimpleDateFormat format = new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss" );
				//getTimeで現在時刻を取得、formatで上の型に直す、endDateはその入れ物
				end = format.format( dateObj.getTime() );
			}

			//UserMessageDaoクラスのselectメソッドに渡す
			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, start, end);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void deleteMessage(int messageId) {

		Connection connection = null;
		try {

			connection = getConnection();

			new MessageDao().deleteMessage(connection, messageId);
			commit(connection);

		}catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void editMessage(Message message) {

		Connection connection = null;
		try {

			connection = getConnection();

			new MessageDao().editMessage(connection, message);
			commit(connection);

		}catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public Message select(int messageid) {

		Connection connection = null;
		try {
			connection = getConnection();
			Message message = new MessageDao().select(connection, messageid);
			commit(connection);
			return message;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}