package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		String id = request.getParameter("messageId");

		Message message = null;
		if(!(id.isEmpty() )|| id.matches("^[0-9]*$")) {
			int messageId = Integer.parseInt(id);
			message = new MessageService().select(messageId);
		}

		if(message == null) {
			session.setAttribute("error", "不正なパラメータが入力されました");
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

		String editText = request.getParameter("text");

		if(StringUtils.isBlank(editText)) {
			session.setAttribute("editError", "入力してください");
			response.sendRedirect("./");
			return;
		}

		if(editText.length() >140) {
			session.setAttribute("editError", "140文字以下で入力してください");
			response.sendRedirect("./");
			return;
		}

		String id = request.getParameter("editId");
		int messageId = Integer.parseInt(id);

		Message message = new Message();
		message.setId(messageId);
		message.setText(editText);

		new MessageService().editMessage(message);

		response.sendRedirect("./");
	}

}
