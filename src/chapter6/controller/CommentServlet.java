package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> commentError = new ArrayList<String>();

		String text = request.getParameter("comment");

		User user = (User) session.getAttribute("loginUser");
		int userId = user.getId();

		String message = request.getParameter("messageId");
		int messageId = Integer.parseInt(message);

		if (!isValid(text, commentError)) {
			session.setAttribute("commentError", commentError);
			response.sendRedirect("./");
			return;
		}

		Comment comment = new Comment();
		comment.setText(text);
		comment.setUserId(userId);
		comment.setMessageId(messageId);

		new CommentService().insert(comment);

		response.sendRedirect("./");
	}

	private boolean isValid(String text, List<String> commentError) {
		if(StringUtils.isBlank(text)) {
			commentError.add("入力してください");
		}else if(text.length() >140) {
			commentError.add("140文字以下で入力してください");

		}
		return true;
	}
}
