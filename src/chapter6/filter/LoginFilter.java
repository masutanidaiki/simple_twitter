package chapter6.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(urlPatterns = {"/edit", "/setting"})
public class LoginFilter implements Filter{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		if(session.getAttribute("loginUser") == null){
			session.setAttribute("messages", "ログインしてください");
			httpResponse.sendRedirect("./login");
			return;
		}
		chain.doFilter(request, response);
	}

	public void destroy() {
	}

	@Override
	public void init(FilterConfig filterConfig) {
	}
}
